add_executable(fibonacci-ordinary  fibonacci/main.cpp  )
add_executable(fibonacci-cached fibonacci/main.cpp  )
add_executable(fibonacci-optimized-cached fibonacci/main.cpp  )
add_executable(generalized-memoization generalized-memoization/main.cpp)
add_executable(lazy-sort lazy-sort/main.cpp)
add_executable(lazy-val lazy-val/main.cpp)
add_executable(recursive-memoization recursive-memoization/main.cpp)
add_executable(string-concatenation string-concatenation/main.cpp)
add_executable(string-concatenation-refs string-concatenation-refs/main.cpp)

set_property(TARGET fibonacci-ordinary   PROPERTY FOLDER "examples/chapter-06")
set_property(TARGET fibonacci-cached  PROPERTY FOLDER "examples/chapter-06")
set_property(TARGET fibonacci-optimized-cached  PROPERTY FOLDER "examples/chapter-06")
set_property(TARGET generalized-memoization   PROPERTY FOLDER "examples/chapter-06")
set_property(TARGET lazy-sort   PROPERTY FOLDER "examples/chapter-06")
set_property(TARGET lazy-val   PROPERTY FOLDER "examples/chapter-06")
set_property(TARGET recursive-memoization   PROPERTY FOLDER "examples/chapter-06")
set_property(TARGET string-concatenation   PROPERTY FOLDER "examples/chapter-06")
set_property(TARGET string-concatenation-refs   PROPERTY FOLDER "examples/chapter-06")

target_compile_definitions(fibonacci-ordinary PRIVATE ORDINARY_FIB)
target_compile_definitions(fibonacci-cached PRIVATE CACHED_FIB)
target_compile_definitions(fibonacci-optimized-cached PRIVATE OPTIMIZED_CACHED_FIB)

set_target_properties(lazy-val PROPERTIES
        CXX_STANDARD 17
        CXX_EXTENSIONS OFF)